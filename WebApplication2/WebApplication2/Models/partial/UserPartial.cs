﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        
    }

    public class UserMetadata
    {
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(5)]
        public string name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Email")]
        [EmailAddress]
        public string E_mail { get; set; }

        public string password { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("生日")]
        public System.DateTime BirthDay { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        public int sex { get; set; }
 
    } 

}