﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(String email ,String pwd)
        {
            Entities bd = new Entities();
            var user = bd.Table_1.Where(d => d.E_mail == email && d.password == pwd).FirstOrDefault();
            if (user != null)
            {

                return RedirectToAction("Index");

            }
            else {
                ViewBag.Message = "密碼錯誤，請重新輸入";
                return View();
            }

        }

	}
}